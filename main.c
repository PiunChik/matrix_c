
#include "matrix.h"

int main_new(){
	MATRIX_TYPE matrix = create_zero_matrix(4,3);

	for (int i =0; i < matrix.width; i++){
		for (int j=0; j < matrix.height; j++){
			printf("%f ", matrix.matrix[i][j]);
		}
		printf("\n");
	}

	return 0;
}