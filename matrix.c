#include "matrix.h"


// Возвращает матрицу с заданными длиной и шириной, заполненную нулями
MATRIX_TYPE create_zero_matrix(int height, int width){
	MATRIX_TYPE result;
	result.height = height;
	result.width = width;

	result.matrix = (double**)malloc(width * sizeof(double*));
	for (int i = 0; i < width; i++)
	{
		result.matrix[i] = (double*)malloc(height * sizeof(double));
		for (int j=0; j < height; j++){
			result.matrix[i][j] = 0;
		}
	}

	return result;
}

// Вывод матрицы в консоль
void output_matrix(MATRIX_TYPE matrix){

}

// Возвращает матрицу, являющуюся результатом умножения двух матриц
MATRIX_TYPE multiply_matrix(MATRIX_TYPE matrix1, MATRIX_TYPE matrix2){
	MATRIX_TYPE result;
	return result;
}
	
// Возвращает новую матрицу, являющуюся транспонированной матрицей для исходной
MATRIX_TYPE get_transpose_matrix(MATRIX_TYPE matrix){
	MATRIX_TYPE result;
	return result;
}
	
// Возвращает новую матрицу, являющуюся суммой двух матриц
MATRIX_TYPE get_summ_matrix(MATRIX_TYPE matrix1, MATRIX_TYPE matrix2){
	MATRIX_TYPE result;
	return result;
}