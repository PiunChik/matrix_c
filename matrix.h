#ifndef MAIN_H
#define MAIN_H

	struct Matrix {
		double** matrix;
		int height;
		int width;
	};
	
	typedef struct Matrix MATRIX_TYPE;

	// Возвращает матрицу с заданными длиной и шириной, заполненную нулями
	MATRIX_TYPE create_zero_matrix(int height, int width);

	// Вывод матрицы в консоль
	void output_matrix(MATRIX_TYPE matrix);

	// Возвращает матрицу, являющуюся результатом умножения двух матриц
	MATRIX_TYPE multiply_matrix(MATRIX_TYPE matrix1, MATRIX_TYPE matrix2);
	
	// Возвращает новую матрицу, являющуюся транспонированной матрицей для исходной
	MATRIX_TYPE get_transpose_matrix(MATRIX_TYPE matrix);
	
	// Возвращает новую матрицу, являющуюся суммой двух матриц
	MATRIX_TYPE get_summ_matrix(MATRIX_TYPE matrix1, MATRIX_TYPE matrix2);


#endif 