#include <stdio.h>
#include <locale.h>

double** mPlus(double** a, double** b, int n, int m)		//���������� �������
{
	double** result;										//������ 2-������ ������
	result = (double**)malloc(n * sizeof(double*));
	for (int i = 0; i < n; i++)
	{
		result[i] = (double*)malloc(m * sizeof(double));
	}

	for (int i = 0; i < n; i++)								//��������������� ���� ��������
	{
		for (int j = 0; j < m; j++)
		{
			result[i][j] = a[i][j] + b[i][j];
		}
	}
	return result;
}

double** mTranc(double** a, int n, int m)					//���������������� ������
{
	double** result;										//� ���� ������� �������� ������� � ��������� �������
	result = (double**)malloc(n * sizeof(double*));
	for (int i = 0; i < n; i++)
	{
		result[i] = (double*)malloc(m * sizeof(double));
	}

	for (int i = 0; i < n; i++)								//��� ���������
	{
		for (int j = 0; j < m; j++)
		{
			result[i][j] = a[j][i];
		}
	}
	return result;
}

double** mMult(double** a, double** b, int n, int m, int k, int q)	//��������� ���� ������ (���� ���������)
{
	double** result;											//�����-������ � ������ ��� � ��������� �������
	result = (double**)malloc(m * sizeof(double*));
	for (int i = 0; i < m; i++)
	{
		result[i] = (double*)malloc(k * sizeof(double));
	}

	for (int i = 0; i < m; i++)									//������ ���������
	{
		for (int j = 0; j < k; j++)
		{
			result[i][j] = 0;
			for (int q = 0; q < n; q++)
			{
				result[i][j] += a[i][q] * b[q][j];
			}
		}
	}
	return result;
}

int main()
{
	setlocale(LC_ALL, "Rus");

	double** a;
	double** b;
	double** summ;
	double** a1;
	double** cMult;

	int n = 3, m = 3, k = 3, q=3;

	a = (double**)malloc(n * sizeof(double*));
	for (int i = 0; i < n; i++)
	{
		a[i] = (double*)malloc(m * sizeof(double));
	}

	b = (double**)malloc(n * sizeof(double*));
	for (int i = 0; i < n; i++)
	{
		b[i] = (double*)malloc(m * sizeof(double));
	}

	a[0][0] = 1; a[0][1] = 2; a[0][2] = 3;
	a[1][0] = 4; a[1][1] = 5; a[1][2] = 6;
	a[2][0] = 7; a[2][1] = 8; a[2][2] = 9;

	b[0][0] = 1; b[0][1] = 0; b[0][2] = 1;
	b[1][0] = 0; b[1][1] = 1; b[1][2] = 0;
	b[2][0] = 1; b[2][1] = 0; b[2][2] = 1;

	summ = mPlus(a, b, n, m);

	a1 = mTranc(a, n, m);

	cMult = mMult(a, b, n, m, k, q);

	printf("������� A:\n");
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%f|", a[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	printf("������� B:\n");
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%f|", b[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	printf("����� ������:\n");
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%f|", summ[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	printf("����������������� ������� �:\n");
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%f|", a1[i][j]);
		}
		printf("\n");
	}

	printf("\n");

	printf("��������� ������ A � B:\n");
	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%f|", cMult[i][j]);
		}
		printf("\n");
	}

	system("pause");
	return 0;
}